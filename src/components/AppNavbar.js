//Import necessary react-bootstrap components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
// import React, { Fragment, useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import { Link, NavLink } from 'react-router-dom';


export default function AppNavbar() {


	return(
		<Navbar bg="dark" variant="dark">
            <Container>
                    <Navbar.Brand as={Link} to="/home">OnlineShop</Navbar.Brand>
                    <Nav className="mr-5">
                        <Nav.Link as={NavLink} to="/home" exact>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/product" exact>Product</Nav.Link>
                        <Nav.Link href="#pricing">Pricing</Nav.Link>
                    </Nav>
            </Container>
        </Navbar>
	)
}