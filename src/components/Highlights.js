import  Img1 from '../images/1.jpeg';
import  Img2 from '../images/2.jpeg';
import  Img3 from '../images/3.jpeg';
import  Img4 from '../images/4.jpeg';
import  Img5 from '../images/5.jpeg';
import  Img6 from '../images/6.jpeg';
import  Img7 from '../images/7.jpeg';
import  Img8 from '../images/8.jpeg';
import { Row, Col, Card, Button, Container } from 'react-bootstrap'

export default function Highlights(){
	return(
        <Container>
            <h1 className='text-center mt-5 mb-4'>All Products</h1>
            <div className='cards'>
                <Row className="mt-3 mb-3">
                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                            <img className = "img-container" src= {Img1} 
                                alt="product1"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Add to Cart</Button>
                            </Card.Body>
                    </Card>
			    </Col>
           
                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img2} 
                                alt="product2"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Add to Cart</Button>
                            </Card.Body>
                    </Card>
			    </Col>

                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img3} 
                                alt="product3"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Go somewhere</Button>
                            </Card.Body>
                    </Card>
			    </Col>

                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img4} 
                                alt="product4"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Go somewhere</Button>
                            </Card.Body>
                    </Card>
			    </Col>
                </Row>

                <Row className="mt-3 mb-3">
                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img5} 
                                alt="product5"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Go somewhere</Button>
                            </Card.Body>
                    </Card>
			    </Col>
           
                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img6} 
                                alt="product6"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Go somewhere</Button>
                            </Card.Body>
                    </Card>
			    </Col>

                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img7} 
                                alt="product7"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Go somewhere</Button>
                            </Card.Body>
                    </Card>
			    </Col>

                <Col xs={11} md={6} lg={3} className = 'mx-0 mb-4'>
                    <Card>
                    <img className = "img-container" src= {Img8} 
                                alt="product8"/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                <Button variant="dark">Go somewhere</Button>
                            </Card.Body>
                    </Card>
			    </Col>
                </Row>
            </div>
		    
        </Container>
		
	)
}